<?php

namespace Vlaosm\CpvUtils\Commands;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vlaosm\CpvCore\Command;

class PackageCommand extends Command
{
    protected function configure() {
        $this
            ->setDescription("Creates new Composer package")
            ->addArgument('name', InputArgument::REQUIRED,
                "Package name ([vendor]/[package])")
            ->addArgument('namespace', InputArgument::REQUIRED,
                "PHP namespace (should end with '\\')")
            ->addArgument('version', InputArgument::REQUIRED,
                "Version of cpv-core module to depend on");
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $name = $input->getArgument('name');
        $namespace = $input->getArgument('namespace');
        $version = $input->getArgument('version');

        $path = BP . "/vendor/{$name}";

        $this->generate("{$path}/composer.json", 'vlaosm/cpv-utils/composer.json', [
            'name' => $name,
            'namespace' => $namespace,
            'version' => $version,
        ]);
    }
}