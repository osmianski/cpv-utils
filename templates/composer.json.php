{
    "name": "<?php echo $name ?>",
    "require": {
        "php": ">=7.1",
        "vlaosm/cpv-core": "<?php echo $version ?>.*"
    },
    "autoload": {
        "psr-4": {
            <?php echo json_encode($namespace) ?>: "src\/"
        }
    },
    "extra": {
        "branch-alias": {
            "dev-master": "<?php echo $version ?>-dev"
        }
    }
}